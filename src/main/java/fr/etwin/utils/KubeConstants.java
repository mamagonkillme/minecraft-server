package fr.etwin.utils;

import org.bukkit.Location;

import java.util.AbstractMap;
import java.util.Map;

/**
 * A Kube Chunk is 256 * 256
 * A Minecraft Chunk is 16 * 16
 * A kube chunk contains 16 * 16 Minecraft chunk
 *
 * A kube zone is 32 * 32
 * A kube chunk contains 8 * 8 zones
 * A kube zone contains 2 * 2 Minecraft chunk
 */
public class KubeConstants {
    public static final int CHUNK_WIDTH = 256;
    public static final int CHUNK_HEIGHT = 32;
    public static final int CHUNK_SIZE = CHUNK_WIDTH * CHUNK_WIDTH * CHUNK_HEIGHT;
    public static final int ZONES_PER_CHUNK = 8;
    public static final int ZONE_WIDTH = CHUNK_WIDTH / ZONES_PER_CHUNK;

    public static final int MINECRAFT_CHUNK_PER_KUBE_CHUNK = KubeConstants.CHUNK_WIDTH / MinecraftConstants.CHUNK_WIDTH;

    public static Coordinate getZoneFromLocation(Location location) {
        return getZoneFromLocation(location.getX(), location.getZ());
    }
    public static Coordinate getZoneFromLocation(double x, double z) {
        int adjustX = x < 0 ? -1 : 0;
        int adjustZ = z < 0 ? -1 : 0;

        double zoneX = Math.floor(x / KubeConstants.ZONE_WIDTH) + adjustX;
        double zoneY = Math.floor(z / KubeConstants.ZONE_WIDTH) + adjustZ;

        return new Coordinate((int)zoneX, (int)zoneY);
    }

    public static Map.Entry<Coordinate, Coordinate> getCoordsFromMinecraftChunk(int mX, int mY) {
        int adjustX = mX < 0 ? -1 : 0;
        int adjustY = mY < 0 ? -1 : 0;

        Coordinate kubeChunkCoords = new Coordinate(
                (mX / MINECRAFT_CHUNK_PER_KUBE_CHUNK) + adjustX,
                (mY / MINECRAFT_CHUNK_PER_KUBE_CHUNK) + adjustY
        );
        Coordinate minecraftChunkInsideKubeChunkCoords = new Coordinate(
                Math.floorMod(mX + adjustX, MINECRAFT_CHUNK_PER_KUBE_CHUNK),
                Math.floorMod(mY + adjustY, MINECRAFT_CHUNK_PER_KUBE_CHUNK)
        );
        return new AbstractMap.SimpleEntry<>(kubeChunkCoords, minecraftChunkInsideKubeChunkCoords);
    }
}
